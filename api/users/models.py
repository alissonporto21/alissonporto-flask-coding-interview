from datetime import datetime 
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy import ForeignKey, inspect

from api.db import Base


class Users(Base):
    __tablename__ = "users"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    password: Mapped[str] = mapped_column(nullable=False)
    email: Mapped[str] = mapped_column(unique=True, nullable=False)
    created_at: Mapped[str] = mapped_column(nullable=False)
    updated_at: Mapped[str] = mapped_column(nullable=False)
    last_login: Mapped[str] = mapped_column(nullable=True)
    first_name: Mapped[str] = mapped_column(nullable=False)
    last_name: Mapped[str] = mapped_column(nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'password': self.password,
            'email': self.email,
            'created_at': self.created_at,
            'updated_at': self.updated_at,
            'last_login': self.last_login,
            'first_name': self.first_name,
            'last_name': self.last_name,
        }



# class Student(Base):
#     __tablename__ = "student"
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     enrollment_date: Mapped[datetime] = mapped_column()
#     min_course_credits: Mapped[int] = mapped_column()
#     first_name: Mapped[str] = mapped_column(nullable=False)
#     last_name: Mapped[str] = mapped_column(nullable=False)
#     user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))


# class Course(Base):
#     __tablename__ = "course"
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     max_students: Mapped[int] = mapped_column()
#     num_credits: Mapped[int] = mapped_column()
#     professor_name: Mapped[str] = mapped_column(nullable=False)
