from typing import Optional

from pydantic import BaseModel

from datetime import datetime 

from flask_openapi3 import APIBlueprint
from flask import request, jsonify
from sqlalchemy import select

from database import db
from api.users.models import Users

users_app = APIBlueprint("users_app", __name__)


class UserSchema(BaseModel):
    id: int
    password: str
    email: str
    created_at: str
    updated_at: str
    last_login: str
    first_name: str
    last_name: str


class UserList(BaseModel):
    users: list[UserSchema]


@users_app.get("/users", responses={"200": UserList})
def get_users():
    with db.session() as session:
        users = session.execute(select(Users)).scalars().all()
        return users
    

@users_app.post("/users", responses={"201": UserList})
def create_users():
    req_data = request.get_json()
    with db.session() as session:
        user = Users(
            password=req_data['password'],
            email=req_data['email'],
            created_at=str(datetime.now()),
            updated_at=str(datetime.now()),
            last_login=str(datetime.now()),
            first_name=req_data['first_name'],
            last_name=req_data['last_name'],
        )
        session.add(user)
        session.commit()
        response = session.execute(select(Users)).first()
        return jsonify(response[0].to_dict()), 201
